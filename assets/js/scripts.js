$(document).ready(function () {

    /********************************  Init Slider Funcion **********************************/
    checkSlider();
    /********************************  Init Slider Funcion **********************************/
    $('a').click(function(e){
        e.preventDefault();
    });
    /********************************  Start Mobile Menu   **********************************/
    $("#ToggleMenu").click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('OpenMenu');
        $("body").toggleClass('HiddenScroll')
    })
    /********************************  End Mobile Menu   **********************************/

    /********************************  Start Window Scroll Actions **********************************/
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 120) {
            $('.goTop').show();
        } else {
            $('.goTop').hide();

        }
        if ($(window).scrollTop() > 120) {
            $('.HeaderSec').addClass('FixedHeader');
        } else {
            $('.HeaderSec').removeClass('FixedHeader');
        }
    });

    $('.goTop').click(function () {
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
        return false;
    });
    /********************************  Start Window Scroll Actions **********************************/

    /********************************  Start Modal **********************************/
    $('.openModal').click(function () {
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
        $('.PopUpContainer').fadeIn();
    });

    $('.CloseModal').click(function () {
        $(this).closest('.PopUpContainer').fadeOut()
    });
    /********************************  End Modal **********************************/
});

 /********************************  Start Page Loader **********************************/
$(window).on('load', function () {
    $("body").addClass('HiddenOverFlow');
    setTimeout(removeLoader, 1000);
});
function removeLoader() {
    $(".Loader").fadeOut(500, function () {
        $(".Loader").remove();
        $("body").removeClass('HiddenOverFlow');
    });
}
/********************************  End Page Loader **********************************/

/********************************  Start checkSlider Funcion **********************************/
function checkSlider() {
    if (window.matchMedia('(min-width: 767px)').matches) {
        $(".center").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true
        })
    } else {
        $('.center').slick({
            arrows: false,
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            centerMode: true,
            autoplay: true,
            autoplaySpeed: 2000,

        });
    }
}
/********************************  End checkSlider Funcion **********************************/

/********************************  Start Init Animation  **********************************/
new WOW().init();
/******************************** End Init Animation **********************************/
